package com.engine;

public interface Disposable {
	public void dispose();
}
