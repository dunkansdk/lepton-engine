package com.engine;

import org.joml.Matrix4f;

import com.engine.graphics.RenderTarget;
import com.engine.graphics.Sprite;
import com.engine.graphics.SpriteBatch;
import com.engine.graphics.Texture;
import com.engine.graphics.TextureRegion;
import com.engine.inputs.Input;
import com.engine.inputs.events.InputEvent;
import com.engine.inputs.events.InputEvent.InputEventSources;
import com.engine.inputs.events.InputEvent.InputEventTypes;
import com.engine.inputs.events.KeyboardInputEvent;
import com.engine.inputs.events.MouseInputEvent;
import com.engine.inputs.listeners.InputListener;
import com.engine.utils.TextureUnit;
import com.engine.utils.Timer;

public class Test implements InputListener {
	
	//The window
	private Window window;
	
	private Sprite sprite;
	private Texture tex;
	
	private int x = 0, y = 0;
	
	private SpriteBatch sb;
	private Matrix4f proj;
	
	@SuppressWarnings("unused")
	private int FPS, frameCounter;
	private Timer timerFPS = new Timer(1000);
	
	boolean apply;
	
	private RenderTarget renderTarget;
	private Sprite sprScreen;
	
	private void createWindow()	{
		//Create the a window and store it in window
		window = new Window(1280, 720, "GLFW");
		window.setVSync(false);
		Input.setInputListener(this);
		
		tex = new Texture("resources/atlas.png", TextureUnit.GL_LINEAR);
		tex.load();
		
		sprite = new Sprite(64, 64, new TextureRegion(tex, 0, tex.getWidth() / 5, tex.getWidth() / 5, tex.getHeight() / 3));
		
		sb = new SpriteBatch(100);
		
		renderTarget = new RenderTarget(1280, 720);
		sprScreen = new Sprite(1280, 720, new TextureRegion(renderTarget.getTexture(), 0, 0, 1280, 720));
		sprScreen.setFlipY(true);
		
		/** refactor {@link graphics.Camera2D} ??? */
		proj = new Matrix4f();
		proj.setOrtho(0, 1280, 720, 0, 1, -1);
		
		sb.setProjection(proj);
		sb.setColor(1f, 1f, 1f, 1f);
	}
	
	private void loop() {
		while(!window.shouldClose()) {
			
			if(apply) {
				sb.dispose();
				window.applyWindowHintsChanges();
				sb = new SpriteBatch(1000);
				sb.setRenderTarget(renderTarget);
				sb.setProjection(proj);
				sb.setColor(1f, 1f, 1f, 1f);
				apply = false;
			}
			
			//TODO: Delta Time: GLFW.glfwSetTime(0);
			
			window.update();
						
			renderTarget.beginAndClear();
						
			/**<b>Note:</b> {@link graphics.RenderTarget} = FrameBufferObject (FBO) */
			sb.setRenderTarget(renderTarget);
			
			sb.begin();
				sb.setRenderTarget(null);
				sb.draw(sprite.getTexRegion(), 0 + x, 0 + y, sprite.getWidth() * 2, sprite.getHeight() * 2, 1, 1, 0, false, false);
			sb.end();
		
			//TODO: Delta Time: GLFW.glfwGetTime();
			
			frameCounter++;
			
			if(timerFPS.tick()) {
				timerFPS.start();
				FPS = frameCounter;
				frameCounter = 0;
			}
		}
	}
	
	public Test() {
		//Create the window
		createWindow();
		
		//Start the game loop
		loop();
		
		tex.dispose();
		sb.dispose();
		window.destroy();
	}

	public static void main(String[] args) {
		new Test();
	}

	@Override
	public void fireInput(InputEvent event) {
		if(event.eventSource == InputEventSources.KEYBOARD) {
			KeyboardInputEvent kEvent = (KeyboardInputEvent) event;
			
			if(kEvent.action == Input.PRESS) {
				if(kEvent.key == Input.KEY_A) x--;
				if(kEvent.key == Input.KEY_S) y++;
				if(kEvent.key == Input.KEY_D) x++;
				if(kEvent.key == Input.KEY_W) y--;
			}
		}
		else if(event.eventSource == InputEventSources.MOUSE) {
			MouseInputEvent mEvent = (MouseInputEvent) event;
			
			if(mEvent.eventType == InputEventTypes.MOUSE_WHEEL) {
				// example				
			}
		}
	}
}