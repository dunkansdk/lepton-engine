package com.engine.inputs.listeners;

import com.engine.inputs.events.KeyboardInputEvent;

public interface KeyboardInputListener {
	public void fireInput(KeyboardInputEvent event);
}
