package com.engine.inputs.listeners;

import com.engine.inputs.events.JoystickInputEvent;

public interface JoystickInputListener {
	public void fireInput(JoystickInputEvent event);
}
