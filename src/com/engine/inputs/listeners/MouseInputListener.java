package com.engine.inputs.listeners;

import com.engine.inputs.events.MouseInputEvent;

public interface MouseInputListener {
	public void fireInput(MouseInputEvent event);
}
