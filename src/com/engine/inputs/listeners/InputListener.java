package com.engine.inputs.listeners;

import com.engine.inputs.events.InputEvent;

public interface InputListener {
	public void fireInput(InputEvent event);
}
