package com.engine;

public interface WindowEventListener {
	public void fireEvent(WindowEvent event);
}
